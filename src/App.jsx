/* eslint-disable react/jsx-key */
import { useState } from 'react'
import { GrFormPrevious, GrFormNext } from 'react-icons/gr'
import { FiSend } from 'react-icons/fi'

import { UserForm } from './components/UserForm'
import { ReviewForm } from './components/ReviewForm'
import { ThanksForm } from './components/ThanksForm'
import { Steps } from './components/Steps'

import { useForm } from './hooks/useForm'

import './App.css'

function App() {
  const [data, setData] = useState({
    name: '',
    email: '',
    review: '',
    comment: ''
  })

  const updateFieldHandler = (key, value) => {
    setData((prev) => {
      return { ...prev, [key]: value }
    })
  }

  const formComponents = [
    <UserForm data={data} updateFieldHandler={updateFieldHandler} />,
    <ReviewForm data={data} updateFieldHandler={updateFieldHandler} />,
    <ThanksForm data={data} />
  ]

  const { currentStep, currentComponent, changeStep, isLastStep, isFirstStep } =
    useForm(formComponents)

  return (
    <div className="app">
      <div className="header">
        <h1>Deixe sua avaliação</h1>
        <p>
          Ficaremos felizes com a sua compra, utilize o formulário abaixo para
          avaliar o produto.
        </p>
      </div>

      <div className="form-container">
        <Steps currentStep={currentStep} />

        <form onSubmit={(e) => changeStep(currentStep + 1, e)}>
          <div className="inputs-container">{currentComponent}</div>

          <div className="actions">
            {!isFirstStep && (
              <button type="button" onClick={() => changeStep(currentStep - 1)}>
                <GrFormPrevious />
                <span>Voltar</span>
              </button>
            )}
            {!isLastStep ? (
              <button type="button" onClick={() => changeStep(currentStep + 1)}>
                <span>Avançar</span>
                <GrFormNext />
              </button>
            ) : (
              <button type="button">
                <span>Enviar</span>
                <FiSend />
              </button>
            )}
          </div>
        </form>
      </div>
    </div>
  )
}

export default App
